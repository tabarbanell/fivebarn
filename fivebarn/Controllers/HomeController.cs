﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace fivebarn.Controllers
{
    public class HomeController : Controller
    {
        [StatsFilter]
        public ActionResult Index()
        {
            ViewBag.Message = "This is the FiveBarn Application.";

            return View();
        }

        [StatsFilter]
        public ActionResult Metrics()
        {
            return View();
        }

        [StatsFilter]
        public ActionResult About()
        {
            ViewBag.Message = "FiveBarn Application. Try at your own risk.";

            return View();
        }

        [StatsFilter]
        public ActionResult Contact()
        {
            ViewBag.Message = "FiveBarn Contacts.";

            return View();
        }
    }
}
