﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using fivebarn.Models;

namespace fivebarn.Controllers
{
    public class MetricsController : Controller
    {
        private ModelsContext db = new ModelsContext();

        //
        // GET: /Metrics/
            [StatsFilterAttribute]
        public ActionResult Index()
        {
            return View(db.Metrics.ToList());
        }

        //
        // GET: /Metrics/Details/5
            [StatsFilterAttribute]
        public ActionResult Details(int id = 0)
        {
            Metrics metrics = db.Metrics.Find(id);
            if (metrics == null)
            {
                return HttpNotFound();
            }
            return View(metrics);
        }

        //
        // GET: /Metrics/Create
            [StatsFilterAttribute]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Metrics/Create

        [HttpPost]
        [StatsFilterAttribute]
        public ActionResult Create(Metrics metrics)
        {
            if (ModelState.IsValid)
            {
                db.Metrics.Add(metrics);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(metrics);
        }

        //
        // GET: /Metrics/Edit/5
            [StatsFilterAttribute]
        public ActionResult Edit(int id = 0)
        {
            Metrics metrics = db.Metrics.Find(id);
            if (metrics == null)
            {
                return HttpNotFound();
            }
            return View(metrics);
        }

        //
        // POST: /Metrics/Edit/5

        [HttpPost]
        [StatsFilterAttribute]
        public ActionResult Edit(Metrics metrics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(metrics).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(metrics);
        }

        //
        // GET: /Metrics/Delete/5
            [StatsFilterAttribute]
        public ActionResult Delete(int id = 0)
        {
            Metrics metrics = db.Metrics.Find(id);
            if (metrics == null)
            {
                return HttpNotFound();
            }
            return View(metrics);
        }

        //
        // POST: /Metrics/Delete/5

        [HttpPost, ActionName("Delete")]
        [StatsFilterAttribute]
        public ActionResult DeleteConfirmed(int id)
        {
            Metrics metrics = db.Metrics.Find(id);
            db.Metrics.Remove(metrics);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}