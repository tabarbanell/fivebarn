﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using fivebarn.Models;

namespace fivebarn.Controllers.Api
{
    public class MetricsController : ApiController
    {
        private ModelsContext db = new ModelsContext();

        // GET api/Metrics
        public IEnumerable<Metrics> GetMetrics()
        {
            return db.Metrics.AsEnumerable();
        }

        // GET api/Metrics/5
        public Metrics GetMetrics(int id)
        {
            Metrics metrics = db.Metrics.Find(id);
            if (metrics == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return metrics;
        }

        // PUT api/Metrics/5
        public HttpResponseMessage PutMetrics(int id, Metrics metrics)
        {
            if (ModelState.IsValid && id == metrics.Id)
            {
                db.Entry(metrics).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Metrics
        public HttpResponseMessage PostMetrics(Metrics metrics)
        {
            if (ModelState.IsValid)
            {
                db.Metrics.Add(metrics);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, metrics);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = metrics.Id }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/MetricsApi/5
        public HttpResponseMessage DeleteMetrics(int id)
        {
            Metrics metrics = db.Metrics.Find(id);
            if (metrics == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Metrics.Remove(metrics);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, metrics);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}