﻿using System;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace fivebarn.Models
{

    public class ModelsContext : DbContext
    {
        public DbSet<Metrics> Metrics { get; set; }
    }

    public class Metrics
    {
        public int Id { get; set; }
        public string DOI { get; set; }
        public string source { get; set; }
        public string metricType { get; set; }

        [DisplayName("Date of Measure")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public DateTime startTime { get; set; }


        public TimeSpan duration { get; set; }
        public long counter { get; set; }
    }
}


