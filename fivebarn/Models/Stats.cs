﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StatsdClient;

namespace fivebarn.Models
{
    public interface IStats
    {
        void count(string counterName);
    }


    public class Stats: IStats
    {
        private static string HOSTNAME = "10.179.66.5";
        private static int PORT = 8125;
        private Statsd s = new Statsd(new StatsdUDP(HOSTNAME, PORT));

        public Stats() { }


        public void count(string counterName)
        {
            s.Send<Statsd.Counting>(counterName, 1);
        }

        public void gauge(string gaugeName, int gaugeValue)
        {
            s.Send<Statsd.Gauge>(gaugeName, gaugeValue);
        }

    }
}