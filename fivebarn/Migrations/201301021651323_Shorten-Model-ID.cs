namespace fivebarn.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShortenModelID : DbMigration
    {
        public override void Up()
        {
            //manually reordered to drop first, insert next
            DropPrimaryKey("dbo.Metrics", new[] { "MetricsId" });
            DropColumn("dbo.Metrics", "MetricsId");

            AddColumn("dbo.Metrics", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Metrics", "Id");
            
        }
        
        public override void Down()
        {
            //manually reordered to drop first, insert next
            DropPrimaryKey("dbo.Metrics", new[] { "Id" });
            DropColumn("dbo.Metrics", "Id"); 
            
            AddColumn("dbo.Metrics", "MetricsId", c => c.Int(nullable: false, identity: true));           
            AddPrimaryKey("dbo.Metrics", "MetricsId");
            
        }
    }
}
